<?php

namespace Drupal\content_remote_options\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\content_remote_options\Plugin\Field\FieldType\ListRemoteOptionsItem;

/**
 * Plugin implementation of the 'basic_string' formatter.
 *
 * @FieldFormatter(
 *   id = "list_remote_options",
 *   label = @Translation("Remote options default"),
 *   field_types = {
 *     "list_remote_options",
 *   },
 *   quickedit = {
 *     "editor" = "plain_text"
 *   }
 * )
 */
class ListRemoteOptionsFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    // Get remote settings (such as endpoint and headers).
    $remote_settings = $this->getFieldSetting('remote');

    // Get field properties to make request response cached for this field.
    $entity_type = $this->fieldDefinition->getTargetEntityTypeId();
    $bundle = $this->fieldDefinition->getTargetBundle();
    $field_machine_name = $this->fieldDefinition->getName();

    $results = ListRemoteOptionsItem::executeRequest($remote_settings, $entity_type, $bundle, $field_machine_name);
    foreach ($items as $delta => $item) {
      // The text value has no text format assigned to it, so the user input
      // should equal the output, including newlines.
      $formatted = $item->value;
      if (!empty($item->value) && !empty($results) && isset($results[$formatted])) {
        $formatted = $results[$formatted];
      }
      $elements[$delta] = [
        '#type' => 'inline_template',
        '#template' => '{{ value|nl2br }}',
        '#context' => ['value' => $formatted],
      ];
    }

    return $elements;
  }

}
