<?php

namespace Drupal\content_remote_options\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'list_remote_options' field type.
 *
 * @FieldType(
 *   id = "list_remote_options",
 *   label = @Translation("List (remote options)"),
 *   description = @Translation("This field stores text values from a list of allowed 'value => label' pairs, obtained from REST webservice."),
 *   category = @Translation("Text"),
 *   default_widget = "list_remote_options_select",
 *   default_formatter = "list_remote_options",
 * )
 */
class ListRemoteOptionsItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'remote' => [],
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {

    $element['remote'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Remote settings'),
    ];

    // Get remote settings (such as endpoint and headers).
    $remote_settings = $this->getSetting('remote');

    $element['remote']['endpoint'] = [
      '#title' => $this->t('Endpoint'),
      '#type' => 'textfield',
      '#description' => $this->t('Endpoint URL.'),
      '#default_value' => !empty($remote_settings) ? $remote_settings['endpoint'] : '',
      '#required' => TRUE,
    ];

    $element['remote']['headers'] = [
      '#title' => $this->t('Headers'),
      '#type' => 'textarea',
      '#description' => $this->t("Example: {'Content-Type': 'application/json'}"),
      '#default_value' => !empty($remote_settings) ? $remote_settings['headers'] : '',
    ];

    $element['remote']['response_key'] = [
      '#title' => $this->t('Response data key'),
      '#type' => 'textfield',
      '#description' => $this->t("Key used to get data items in response data. Leave it empty if it is not necessary. Example: {'contentResponse' : [{'key' : 'key', 'value' : 'value'}, {'key' : 'key2', 'value' : 'value2'}] -> In this case, you should use <b>contentResponse</b>. To use response key in child levels of JSON array, you should use '.' to navigate to child levels (Example: contentResponse.data.items)"),
      '#default_value' => !empty($remote_settings) ? $remote_settings['response_key'] : '',
    ];

    $element['remote']['data_key'] = [
      '#title' => $this->t('Response items key'),
      '#type' => 'textfield',
      '#description' => $this->t("Key used in select items. Leave it empty if expected response data is not associative array. Example with associative array: if response data is [{'key' : 'key', 'value' : 'value'}, {'key' : 'key2', 'value' : 'value2'}], key should be <b>key</b> and value should be <b>value</b>."),
      '#default_value' => !empty($remote_settings) ? $remote_settings['data_key'] : '',
    ];

    $element['remote']['data_value'] = [
      '#title' => $this->t('Response items value'),
      '#type' => 'textfield',
      '#description' => $this->t("Value used in select items. Leave it empty if expected response data is not associative array. Example with associative array: if response data is [{'key' : 'key', 'value' : 'value'}, {'key' : 'key2', 'value' : 'value2'}], key should be <b>key</b> and value should be <b>value</b>."),
      '#default_value' => !empty($remote_settings) ? $remote_settings['data_value'] : '',
    ];

    $element['remote']['use_empty_option'] = [
      '#title' => $this->t('Use empty option'),
      '#type' => 'checkbox',
      '#description' => $this->t("Use empty option used as default option in select."),
      '#default_value' => !empty($remote_settings) ? $remote_settings['use_empty_option'] : '',
    ];

    $element['remote']['empty_option'] = [
      '#title' => $this->t('Empty option label'),
      '#type' => 'textfield',
      '#states' => ['visible' => [':input[name="properties[use_empty_option]"]' => ['checked' => TRUE]]],
      '#default_value' => !empty($remote_settings) ? $remote_settings['empty_option'] : '',
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Remote option value'))
      ->addConstraint('Length', ['max' => 255])
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
      'indexes' => [
        'value' => ['value'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * Execute request to get options from endpoint.
   *
   * @param array $options
   *   Field options.
   * @param string $entity_type
   *   Entity type to which field is attached.
   * @param string $bundle
   *   Bundle to which field is attached.
   * @param string $field_machine_name
   *   Field machine name.
   *
   * @return array
   *   Result obtained from request.
   */
  public static function executeRequest(array $options = [], $entity_type = '', $bundle = '', $field_machine_name = '') {

    // Get cached response.
    $cid = '';
    if (!empty($entity_type) && !empty($bundle) && !empty($field_machine_name)) {
      $cid = 'content_remote_options:' . $entity_type . ':' . $bundle . ':' . $field_machine_name . ':' .
        \Drupal::languageManager()->getCurrentLanguage()->getId();
      $data = NULL;
      if ($cache = \Drupal::cache()->get($cid)) {
        $data = $cache->data;
        return $data;
      }
    }

    $result = [];
    $client = \Drupal::httpClient();

    // Hook to alter options
    $context = [
      'entity_type' => $entity_type,
      'bundle' => $bundle,
      'field_name' => $field_machine_name,
    ];

    \Drupal::moduleHandler()->alter('content_remote_options__options', $options, $context);

    // Get endpoint used to get options.
    $endpoint = $options['endpoint'] ?? '';
    if (empty($endpoint)) {
      return [];
    }

    if (strpos($endpoint, '/') === 0) {
      $host = \Drupal::request()->getSchemeAndHttpHost();
      $endpoint = $host . $endpoint;
    }

    $data_key = $options['data_key'] ?? '';
    $data_value = $options['data_value'] ?? '';

    $headers = isset($options['headers']) && !empty($options['headers']) ? json_decode(
      str_replace(
        [
          "\r",
          "\n",
        ],
        '',
        $options['headers']
      ), TRUE) : '';
    try {
      $response = $client->get($endpoint, [
        'headers' => !empty($headers) ? $headers : [],
      ]);

      $body = $response->getBody();
      if (empty($body)) {
        return [];
      }
      $data = $body->getContents();

      if (empty($data)) {
        return [];
      }
      else {
        $data = json_decode($data, TRUE);
        if (isset($data_key) && !empty($data_key)) {
          $data = self::getJsonValue($data, $data_key);
        }
        if (!self::isAssoc($data)) {
          if (!is_array($data[0])) {
            foreach ($data as $item) {
              $result[$item] = $item;
            }
          }
          else {
            if (self::isAssoc($data[0]) && !empty($data_key) && !empty($data_value)) {
              foreach ($data as $item) {
                $result[$item[$data_key]] = $item[$data_value];
              }
            }
            else {
              return [];
            }
          }
        }
        else {
          foreach ($data as $item) {
            if (is_array($item)) {
              $value = $item[$data_value];
            }
            else {
              $value = $item;
            }
            $result[$item[$data_key]] = $value;
          }
        }
      }

      // Save results in cache.
      if (!empty($cid)) {
        \Drupal::cache()->set($cid, $result);
      }
      return $result;
    }
    catch (\Exception $e) {
      return [];
    }
  }

  /**
   * Gets value from JSON array.
   *
   * @param array $retval
   *   Original array from which the value is obtained.
   * @param string $string
   *   Path to value.
   *
   * @return string
   *   Result value from specific key/path inside the JSON array.
   */
  private static function getJsonValue(array $retval, $string) {
    $parts = explode('.', $string);
    foreach ($parts as $part) {
      $retval = (array_key_exists($part, $retval) ? $retval[$part] : $retval);
    }
    return $retval;
  }

  /**
   * Check if array is associative.
   *
   * @param array $arr
   *   Array to be checked.
   *
   * @return bool
   *   True if array is associative, otherwise return false.
   */
  private static function isAssoc(array $arr) {
    if ([] === $arr) {
      return FALSE;
    }
    return array_keys($arr) !== range(0, count($arr) - 1);
  }

}
