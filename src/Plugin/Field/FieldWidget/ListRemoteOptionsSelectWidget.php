<?php

namespace Drupal\content_remote_options\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\content_remote_options\Plugin\Field\FieldType\ListRemoteOptionsItem;

/**
 * Content Remote Options Select widget.
 *
 * @FieldWidget(
 *   id = "list_remote_options_select",
 *   label = @Translation("Select with remote options"),
 *   field_types = {
 *     "list_remote_options"
 *   }
 * )
 */
class ListRemoteOptionsSelectWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['value'] = $element + [
      '#type' => 'select',
      '#default_value' => $items[$delta]->value ?? NULL,
      '#attributes' => [
        'class' => [
          'js-list-remote-options',
          'list-remote-options',
        ],
      ],
    ];

    // Get remote settings (such as endpoint and headers).
    $remote_settings = $this->getFieldSetting('remote');
    if (empty($remote_settings)) {
      return $element;
    }

    // Get field properties to make request response cached for this field.
    $entity_type = $this->fieldDefinition->getTargetEntityTypeId();
    $bundle = $this->fieldDefinition->getTargetBundle();
    $field_machine_name = $this->fieldDefinition->getName();

    $element['value']['#options'] = ListRemoteOptionsItem::executeRequest($remote_settings, $entity_type, $bundle, $field_machine_name);
    $use_empty_option = $remote_settings['use_empty_option'];
    if (empty($use_empty_option)) {
      return $element;
    }

    $empty_option = $remote_settings['empty_option'];
    if (empty($empty_option)) {
      $required = $this->fieldDefinition->isRequired();
      $empty_option_text = $required ? $this->t('- Select -') : $this->t('- None -');
    }
    else {
      $empty_option_text = $empty_option;
    }

    $element['value']['#empty_option'] = $empty_option_text;

    return $element;
  }

}
