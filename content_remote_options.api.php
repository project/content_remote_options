<?php

/**
 * @file
 * Hooks provided by the Content Remote Options module.
 */

/**
 * @content_remote_options hooks
 * @{
 */

/**
 * Alter options.
 *
 * @param array $options
 *   Content remote options field settings
 * @param array $context
 *   Context with the entity type, bundle and field machine name
 */
function hook_content_remote_options__options_alter(array &$options, array $context) {
    if($context['bundle'] === 'my_bundle'){
      $options['endpoint'] = 'https://example.com/custom-endpoint';
    }
}

/**
 * @} End of "content_remote_options hooks".
 */
