<?php

namespace Drupal\content_remote_options_test\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Return sample JSON data used for testing.
 */
class ExampleDataController {

  /**
   * Return data used for testing purposes.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Example json data for testing purposes.
   */
  public function test() {
    return new JsonResponse([
      [
        'id' => 1,
        'title' => 'test1',
      ],
      [
        'id' => 2,
        'title' => 'test2',
      ],
    ]);
  }

}
