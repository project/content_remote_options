<?php

namespace Drupal\Tests\content_remote_options\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests for content remote options.
 *
 * @group content_remote_options
 */
class ContentRemoteOptionsTest extends BrowserTestBase {

  /**
   * A user with permission to administer content.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $authorizedUser;

  /**
   * A user without permission to administer content.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $unauthorizedUser;

  /**
   * The administration form path.
   *
   * @var string
   */
  protected $adminFormPath;

  /**
   * Set default theme to stable.
   *
   * @var string
   */
  protected $defaultTheme = 'stable';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'content_remote_options',
    'content_remote_options_test',
    'field',
    'menu_ui',
    'node',
    'path',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->authorizedUser = $this->createUser(
      [
        'administer nodes',
        'bypass node access',
      ],
      'acadmin'
    );
    $this->unauthorizedUser = $this->createUser([], 'nonadmin');
    $this->adminFormPath = '/node/add/content_remote_options_bundle';
  }

  /**
   * Tests content remote options element.
   */
  public function testContentRemoteOptionsField() {
    $assert_session = $this->assertSession();
    $this->drupalLogin($this->unauthorizedUser);
    $this->drupalGet($this->adminFormPath);
    $assert_session->statusCodeEquals(403);
    $this->drupalLogin($this->authorizedUser);
    $this->drupalGet($this->adminFormPath);
    $assert_session->statusCodeEquals(200);
    $assert_session->responseContains('<option value="1">test1</option>');
  }

}
